*******
Vorwort
*******

Diese Anleitung soll Sie in die Lage versetzen, die Handhabung Ihres Computers und die
Anwendung des Praxis-Programms schrittweise anhand von praktischen Beispielen zu erlernen.
Gleichzeitig soll sie auch als ständiges Nachschlagewerk für den täglichen Gebrauch dienen. Gesucht - Gefunden!
Sollten Sie also einmal in einer konkreten Situation nicht mehr weiter wissen, so steht Ihnen das
folgende Inhaltsverzeichnis, als auch das sich am Ende dieser Bedienungsanleitung befindende Inhaltsverzeichnis
Stichwortregister zum schnellen Auffinden des gewünschten Abschnittes zur Verfügung. Stichwortregister
Beachten Sie ebenso das Tabellenverzeichnis, sowie im Besonderen den Anhang mit u.A. Tabellenverzeichnis
der FAQ, in denen ggf. der gesuchte Sachverhalt anschaulicher bzw. kurz und übersichtlich Anhang
gegliedert dargestellt ist.

Diese Knöpfe sind auf den nachfolgenden Seiten stets am unteren Blattrand in der Fußzeile
zu sehen: [Index Inhalt FAQ Update Suche Hilfe]

Somit können Sie jederzeit direkt zum Stichwortverzeichnis (Index), Inhaltsverzeichnis (In-
halt), dem aktuellen Updateschreiben (Update) und in die Volltextsuche (Suche, dieser Knopf
funktioniert nur im Acrobat Reader) springen.
Um dieses Handbuch optimal nutzen zu können, sollten Sie in jedem Fall einen Blick in den
kurzen, aber informativen Aufbau und Nutzung des Handbuchs“, werfen,
es lohnt sich!

Die dortigen Hilfestellungen zum Handbuch können ebenfalls auf jeder der nachfolgenden
Seiten über den oben zu sehenden letzten Knopf (Hilfe) aufgerufen werden.
Das Vorhandensein sowie Zutreffen der in dieser Anleitung enthaltenen Programm-
funktionen in der beschriebenen Art und Weise hängt jedoch ganz wesentlich von der
jeweils aktuellen Programmversion Ihres Rechners ab.
Die Anwendung einiger Funktionen kann daher im Einzelfall von der vorliegenden Bedienungs-
anleitung abweichen.

Im Zweifelsfall ist daher immer die im Praxis-Programm eingebaute elektronische Online-
Hilfe für die installierte Version gültig, und sollte also vorgezogen werden. Dies empfiehlt
sich übrigens auch schon allein deswegen, weil die elektronische Online-Hilfe jederzeit auf
Knopfdruck (Funktionstaste ) stets die „richtige Seite“ aufschlägt...
Zusätzlich können Infos zu „brandheißen“ Neuerungen stets im Menü
Hilfe | Letzte Änderungen eingesehen werden, und zwar in umgekehrter chronologischer
Reihenfolge, mit den neuesten Programmänderungen zu Beginn.
