.. Testdokumentation documentation master file, created by
   sphinx-quickstart on Fri Jan 29 10:28:11 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Das medisoftware Praxis-Programm Handbuch
=========================================

Inhalt:

.. toctree::
   :maxdepth: 2

   pages/vorwort
   pages/allgemeines
   pages/startundhife
   pages/zentralefunktionen
   pages/patientenverwaltung
   pages/karteikarte
   pages/kassenleistung
   pages/kassenabrechnung
   pages/statistik
   pages/rechnungen
   pages/hintergrunddatenbanken
   pages/medikamentendatenbank
   pages/adresskartei
   pages/mediword
   pages/praxisorga
   pages/kommunikation



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
